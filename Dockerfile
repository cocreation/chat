FROM node:12.13.0-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY . .

USER node

RUN yarn install
RUN yarn build
CMD ["rm", "-rf", "src"]

COPY --chown=node:node . .

CMD [ "npm", "run", "start"]
