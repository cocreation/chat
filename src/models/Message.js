import mongoose from 'mongoose';

const MessageSchema = mongoose.Schema({
    roomId: {
        type: mongoose.ObjectId,
        required: true,
    },
    message: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});


export default mongoose.model('Message', MessageSchema);