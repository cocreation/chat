import express from 'express';
import socketIo from 'socket.io';
import http from 'http';
import config from './config';
import Message from "./models/Message";
import dbConnection from './db';
import cors from 'cors';

const app = express();
const applicationHandler = http.createServer(app);
const io = socketIo(applicationHandler);


dbConnection(db => {
    app.use(
        cors({
            origin: '*',
            methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        }),
    );
    app.get('/', (req, res) => {
        res.sendFile(__dirname + '/public/index.html')
    });
    app.get('/messages/:roomId', (req, res) => {
        Message.find({roomId: req.params.roomId})
            .then(messages => {
                res.status(200)
                    .json({
                        isSuccess: true,
                        message: 'Messages',
                        data: messages
                    });
            }).catch(error => {
            res.status(500)
                .json({
                    isSuccess: false,
                    message: error.message,
                    data: error
                });
        });
    });
    const chat = io
        .of('/chat')
        .on('connection', (socket) => {
            console.log(`a user connected ${socket.id}`);
            socket.on('disconnect', () => {
                console.log('user disconnected');
            });
            socket.on('sendMessage', ({message, roomId}) => {
                const msg = new Message({roomId, message});
                msg.save()
                    .then(createdMsg => {
                        console.log('message: ' + message, 'room ' + roomId);
                        chat.emit(roomId, createdMsg);
                    });
            });
        })

})


applicationHandler.listen(config.applicationPort, () => {
    console.log(`Listening on ${config.applicationPort}`);
})