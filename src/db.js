import mongoose from 'mongoose';
import config from './config';
import BlueBird from 'bluebird';


export default callback => {
    let db = mongoose.connect(config.mongoUrl, {
        promiseLibrary: BlueBird,
        useNewUrlParser: true,
        useCreateIndex: true,
    });

    callback(db);
};
